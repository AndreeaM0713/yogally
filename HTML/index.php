<?php include('head.php'); ?>

<body>


	<main>
		<!-- PAGE CONTENT
		============================================= -->
		<div id="page" class="page">
			<?php include('header.php'); ?>

			<?php include('hero.php'); ?>

			<!-- ABOUT-1
	============================================= -->
			<section id="about" class="about-section division about-container-right">
				<div class="txt-block right-column p-4 about-section-text" style="background-color: white; box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;">

					<!-- Title -->
					<h3 class="h4-md txt-color-01">Cine sunt?</h3>

					<!-- Text -->
					<p class="txt-color-05">De esti nou pe pagina mea sau ma urmaresti de ceva timp, imi pare bine sa ma prezint.
						In urma cu vreo 10 ani, mintea mea nu mai era prietena cu mine asa ca am ales metode si metode sa ma readuc la viata (mai multi ani si mai multe tehnici - psihoterapie, terapie florala, hipnoza, acupunctura, constelatii familiale, regresii etc.).
					</p>

					<!-- Text -->
					<p class="txt-color-05">
						Singura care a reusit sa o faca bland si din prima a fost insasi Yoga. A inserat in interiorul fiintei mele un echilibru de neegalat intr-un haos fara margini.
					</p>

					<h3 class="h4-md txt-color-01">De ce decizii “nebunesti”?</h3>

					<!-- Text -->
					<ul style="list-style-type: disc">
						<li>
							<p class="txt-color-05">
								Pentru ca am ales sa renunt la un job platit cu cateva mii de euro in fiecare luna, pentru cateva mii de lei.
							</p>
						</li>
						<li>
							<p class="txt-color-05">
								Pentru ca am ales sa investesc mii de euro intr-un domeniu novice (pana acum 2-3 ani) in tara noastra, intr-un domeniu nou inclusiv pentru mine.
							</p>
						</li>
						<li>
							<p class="txt-color-05">
								Pentru ca am ales sa imi urmez inima si nu mintea, asa cum suntem cu totii invatati sa o facem.
							</p>
						</li>
					</ul>
				</div>
			</section> <!-- END ABOUT-1 -->




			<!-- ABOUT-2
	============================================= -->
			<section id="about-1" class="about-section division about-container-left">
				<div class="txt-block right-column p-3 about-section-text" style="background-color: white; box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;">

					<h3 class="h4-md txt-color-01">Cum am ajuns de la practicant la profesor?</h3>

					<p class="h4-md txt-color-05">
						De 10 ani sunt studenta in practica Yoga si sunt in continuare mirata de minunatiile ei si cum ele se manifesta prin menirea mea.
						Terapeut in tehnica Theta Healing<sup>®</sup>, a venit manusa la ceea ce faceam deja.
						Multi dintre cursanti doreau sesiuni private de yoga insa povestile lor aveau nevoie de mai multa informatie, cunoastere, asumare si responsabilitate din partea mea.
					</p>
					<p class="h4-md txt-color-05">
						Theta Healing<sup>®</sup> a venit de fapt la “cererea” cursantilor/clientilor mei.
						Theta Healing<sup>®</sup> a venit si din nevoia mea, incat sa nu mai bajbai, sa stiu exact ce, cum, sa fie viata mea, profesia mea.
						In viata personala, sunt mama unui baiat de 2 ani si cateva luni, sunt iubita si iubesc un barbat desenat dupa tot ce era necesar si optim pentru mine, sufletul meu pereche.
					</p>
					<p class="h4-md txt-color-05">
						Aceasta sunt eu, abia astept sa te cunosc si pe tine.
					</p>
					<p class="h4-md txt-color-05 font-italic mt-5">
						Semnat: omul, mama si terapeutul.
					</p>
				</div>
			</section> <!-- END ABOUT-1 -->




			<!-- SERVICES-13
			============================================= -->
			<section id="thetahealing" class="bg-color-01 services-section division">
				<div class="container">


					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-lg-10 offset-lg-1 theta">
							<div class="section-title mb-60 text-center">

								<!-- Transparent Header -->
								<h2 class="tra-header txt-color-02">Ce este?</h2>

								<!-- Title 	-->
								<h3 class="h3-xl txt-color-05">Theta Healing</h3>

								<!-- Text -->
								<p class="p-lg txt-color-05">
									O metoda eficienta folosita pentru vindecare fizica, emotionala si spirituala.
									Theta Healing<sup>®</sup> este calea prin care tu te conectezi la Sursa Vietii, la Energia Creatoare,
									la Divinitate sau cum preferi sa o numesti, dincolo de corpul fizic.
								</p>
							</div>
						</div>
						<img src="./images/image9.webp" alt="" class="w-100 d-xl-none">

					</div>


					<!-- SERVICES-13 WRAPPER -->
					<div class="sbox-13-wrapper">
						<div class="row equal-height-cards" style="padding: 0.5rem;">
							<!-- SERVICE BOX #1 -->
							<div class="col-12 col-lg-6 my-md-3 my-3">
								<div class="bg-fixed bg-color-02 service-box">
									<div class="sbox-13-txt">
										<!-- Title -->
										<h5 class="h5-xl txt-color-05">Accesarea Subconstientului</h5>
										<!-- Text -->
										<p class="txt-color-05 text-justify">
											Aceasta tehnica ne permite sa aducem mintea intr-o liniste profunda,
											reducand frecventele cerebrale la starea Theta (4-7 Hz),
											accesand astfel subconstientul, unde incep schimbarile.
											Folosim mintea constienta doar 10%, iar subconstientul, responsabil de ganduri, emotii si actiuni, peste 90%.
											Orice informatie repetata intra in subconstient si devine credinta noastra, realitatea noastra.
										</p>
									</div>
								</div>
							</div> <!-- END SERVICE BOX #1 -->

							<!-- SERVICE BOX #2 -->
							<div class="col-12 col-lg-6 my-md-3 my-3">
								<div class="bg-fixed bg-color-02 service-box">
									<div class="sbox-13-txt">
										<!-- Title -->
										<h5 class="h5-xl txt-color-05">Eliminarea Credintelor</h5>
										<!-- Text -->
										<p class="txt-color-05 text-justify">
											Multe dintre credintele noastre limitative ne fac sa consideram normala trairea cu anxietate,
											boli fizice, relatii defectuoase, saracie financiara sau educationala, resentimente si tristete.
											Prin Tehnica Theta Healing<sup>®</sup>, accesand subconstientul, ajungem la "sursa individuala",
											unde putem modifica si anula credintele vechi si nefolositoare, adaugand unele noi care aduc evolutie si crestere personala.
										</p>
									</div>
								</div>
							</div> <!-- END SERVICE BOX #2 -->

							<!-- SERVICE BOX #3 -->
							<div class="col-12 col-lg-6 my-md-3 my-3">
								<div class="bg-fixed bg-color-02 service-box">
									<div class="sbox-13-txt">
										<!-- Title -->
										<h5 class="h5-xl txt-color-05">Beneficiile Terapiei</h5>
										<!-- Text -->
										<p class="txt-color-05 text-justify">
											Terapia Theta Healing<sup>®</sup> este recunoscuta international,
											avand milioane de terapeuti acreditati care aduc zilnic bunastare
											si claritate celor ce aleg sa traiasca cu bucurie.
											Clientii aleg aceasta terapie pentru a cauta si elibera cauzele anxietatii,
											atacurilor de panica, depresiei, insomniei, si pentru a vindeca relatiile familiale,
											fie cu parintii, fie cu copiii.
										</p>
									</div>
								</div>
							</div> <!-- END SERVICE BOX #3 -->

							<!-- SERVICE BOX #4 -->
							<div class="col-12 col-lg-6 my-md-3 my-3">
								<div class="bg-fixed bg-color-02 service-box">
									<div class="sbox-13-txt">
										<!-- Title -->
										<h5 class="h5-xl txt-color-05">Eliberare si Crestere</h5>
										<!-- Text -->
										<p class="txt-color-05 text-justify">
											Eliberarea de relatii nefolositoare (persoane, foste locuri de munca, locuinte, animale, persoane decedate);
											renuntarea la adictii sau vicii (inclusiv pentru persoanele cu kilograme suplimentare); imbunatatirea relatiei cu banii,
											crestere financiara si profesionala, eliberarea starii de saracie; gasirea sufletului pereche si imbunatatirea relatiei de cuplu;
											teama de a vorbi in public; cauzele bolilor fizice, fricilor si fricii de moarte. Insa mai exista si alte aspecte la Tehnica Theta Healing,
											cauta si gaseste rezolvarea incat tu sa fii liber/a sa traiesti o viata plina de sens si plina de curaj.
										</p>
									</div>
								</div>
							</div> <!-- END SERVICE BOX #4 -->
						</div> <!-- End row -->
					</div> <!-- END SERVICES-13 WRAPPER -->



				</div> <!-- End container -->
			</section> <!-- END SERVICES-13 -->




			<!-- Sesiune Privata Theta Healing pentru Anxietate, Atacuri de panica, Depresie, Insomnie -->
			<section id="services-1" class="anxiety bg-color-02 services-section division">
				<div class="container">
					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-xl-5"></div>
						<div class="col-lg-7 text-center">
							<div class="section-title mb-60 text-center">
								<!-- Transparent Header -->
								<h2 class="tra-header txt-color-02">Sesiune Privata</h2>
								<!-- Title -->
								<h3 class="h3-xl txt-color-05">Anxietate, Atacuri de panica,</h3>
								<h3 class="h3-xl txt-color-05">Depresie, Insomnie</h3>
								<!-- Text -->
								<p class="p-lg txt-color-05 text-justify">
									Anxietatea este inca cel mai des motiv pentru care clientii aleg sesiunile de terapie.
									Bineinteles aceste reactii au un scop initial bun de a ne mentine intr-o stare de confort si siguranta,
									insa atunci cand devin coplesitoare si ne influenteaza calitatea vietii inseamna ca e momentul sa intervenim.
								</p>
								<!-- More Text (initially hidden) -->
								<div id="more-text" style="display: none;">
									<p class="p-lg txt-color-05 text-justify">
										Iti pun la dispozitie toate informatiile teoretice, insa si practice
										(incercand aceasta metoda pe propria anxietate pe care nu reuseam sa o temperez prin nicio metoda)
										pentru ca sa gasim impreuna cauzele subconstiente ce mentin aceste dereglari in viata ta.
										Iti voi oferi confidentialitate, respect si lipsa de judecat, incat sa accesam in propriul tau ritm adevarul vietii tale.
									</p>
									<p class="p-lg txt-color-05 text-justify">
										Sesiunile se pot sustine atat online, cat si in studioul Yogally din Constanta.
										Tin sa iti multumesc pentru incredere si te felicit in avans pentru decizia de a face pasi spre eliberare.
									</p>
								</div>
								<!-- "Afla mai multe" Link -->
								<p class="p-lg txt-color-05 text-justify">
									<a href="#" id="read-more">...Afla mai multe</a>
								</p>
							</div>
							<a class="btn btn-md btn-color-02 tra-02-hover privateSesionBtn" aria-label="WhatsApp" href="#contact-form" rel="noopener nofollow" data-subject="Theta Healing pentru Anxietate, Atacuri de panica, Depresie, Insomnie">Programeaza-te</a>
						</div>
					</div>
				</div> <!-- End container -->
			</section> <!-- END Sesiune Privata Theta Healing pentru Anxietate, Atacuri de panica, Depresie, Insomnie -->




			<!-- Sesiune privata Theta Healing si Divortul Energetic -->
			<section id="services-1" class="divorce bg-color-01 pb-60 services-section division">
				<div class="container">
					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-lg-7 text-center">
							<div class="section-title mb-60 text-center">
								<!-- Transparent Header -->
								<h2 class="tra-header txt-color-02">Sesiune Privata</h2>
								<!-- Title -->
								<h3 class="h3-xl">Divortul Energetic</h3>
								<!-- Text -->
								<p class="p-lg text-justify">
									Divortul Energetic este metoda ideala de a desface, rupe, elibera acele cordoane si atasamente de energie nebenefice sufletului tau, construite intre tine si orice persoana, obiect, animal, locatie, emotie, etc., cu care tu ai creat o relatie.
									Atunci cand intri in contact cu o persoana, animal, loc de munca, emotie, etc., creezi o relatie, indiferent de durata sau tipul ei.
								</p>
								<!-- More Text (initially hidden) -->
								<div id="more-text-2" style="display: none;">
									<p class="p-lg text-justify">
										In momentul in care aceste legaturi energetice nu mai servesc binelui sufletului tau este momentul sa intervenim prin ruperea legaturilor de energie.
										Prin eliberarea atasamentelor nefolositoare tu inchei etapele vechi, iti redobandesti puterea, energia, respectul si vei atrage oportunitati deosebite.
										Divortul energetic este utilizat in separarea de energie fata de membri ai familiei, parteneri, parteneri sexuali, colegi, persoane decedate, locuri de munca, vechi locuinte (apartamente), animale, situatii, kilograme, mancare, etc.
									</p>
									<p class="p-lg text-justify">
										Vom lucra profund cu credintele limitative si vom gasi cauzele subconstiente ce te mentin legat(a) in acea relatie.
										Iti voi oferi confidentialitate, respect si lipsa de judecat, incat sa accesam in propriul tau ritm adevarul vietii tale.
										Sesiunile se pot sustine atat online, cat si in studioul Yogally din Constanta.
										Tin sa iti multumesc pentru incredere si te felicit in avans pentru decizia de a face pasi spre eliberare.
									</p>
								</div>
								<!-- "Afla mai multe" Link -->
								<p class="p-lg text-justify">
									<a href="#" id="read-more-2">...Afla mai multe</a>
								</p>
							</div>
							<a class="btn btn-md btn-color-02 tra-02-hover privateSesionBtn" aria-label="WhatsApp" href="#contact-form" rel="noopener nofollow" data-subject="Theta Healing si Divortul Energetic">Programeaza-te</a>
						</div>
						<div class="col-xl-5"></div>
					</div>
				</div> <!-- End container -->
			</section> <!-- END Sesiune privata Theta Healing si Divortul Energetic -->

			<img src="./images/image10.webp" alt="" class="w-100 d-xl-none">


			<!-- Sesiune Privata Theta Healing si Atragerea sufletului pereche potrivit si optim -->
			<section id="services-1" class="partner bg-color-01 pb-60 services-section division">
				<div class="container">
					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-xl-5"></div>
						<div class="col-lg-7 text-center">
							<div class="section-title mb-60 text-center">
								<!-- Transparent Header -->
								<h2 class="tra-header txt-color-02">Sesiune Privata</h2>
								<!-- Title -->
								<h3 class="h3-xl txt-color-05">Atragerea sufletului pereche</h3>
								<!-- Text -->

								<div class="ul" style="font-weight: bold;">
									<ul class="p-lg txt-color-05 text-justify mx-3" style="list-style-type: circle">
										<li>Este iubirea pentru tine?</li>
										<li>Crezi ca meriti iubirea?</li>
										<li>Crezi ca poti sa traiesti in si cu iubire?</li>
										<li>Oare exista cineva potrivit pentru tine?</li>
									</ul>
								</div>

								<!-- Text -->
								<p class="p-lg txt-color-05 text-justify">
									In zecile de sesiuni pe care le-am sustinut in tehnica Theta Healing, atat cu persoane singure, divortate sau chiar in relatii (casnicii), faptul ca aveau cauze subconstiente ce sustineau starea de tristete, nefericire, amaraciune, atrageau dupa sine atat singuratatea cat si relatii defectuoase sau nepotrivite.
								</p>
								<!-- More Text (initially hidden) -->
								<div id="more-text-3" style="display: none;">
									<p class="p-lg txt-color-05 text-justify">
										Pot sa iti confirm ca indiferent de alegi sa traiesti singur/a sau la brat cu altcineva, viata in iubire este doar o alegere personala si este posibila si pentru tine.
										Prin aceasta sesiune-calatorie vei redescoperi starea de iubire, starea de renastere, starea prin care te vei elibera de toate blocajele emotionale.
									</p>
									<p class="p-lg txt-color-0 text-justify">
										Iti voi oferi confidentialitate, respect si lipsa de judecat, incat sa accesam in propriul tau ritm adevarul vietii tale.
										Sesiunile se pot sustine atat online, cat si in studioul Yogally din Constanta.
										Tin sa iti multumesc pentru incredere si te felicit in avans pentru decizia de a face pasi spre eliberare.
									</p>
								</div>
								<!-- "Afla mai multe" Link -->
								<p class="p-lg txt-color-05 text-justify">
									<a href="#" id="read-more-3">...Afla mai multe</a>
								</p>
							</div>
							<a class="btn btn-md btn-color-02 tra-02-hover privateSesionBtn" aria-label="WhatsApp" href="#contact-form" rel="noopener nofollow" data-subject="Theta Healing si Atragerea sufletului pereche potrivit si optim">Programeaza-te</a>
						</div>
					</div>
				</div> <!-- End container -->
			</section> <!-- END Sesiune Privata Theta Healing si Atragerea sufletului pereche potrivit si optim -->

			<img src="./images/image001415.webp" alt="" class="w-100 d-xl-none">



			<!-- Sesiune Privata Theta Healing pentru misiunea de viata si manifestare -->
			<section id="services-1" class="mission bg-color-01 pb-60 services-section division">
				<div class="container">
					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-lg-7 text-center">
							<div class="section-title mb-60 text-center">
								<!-- Transparent Header -->
								<h2 class="tra-header txt-color-02">Sesiune Privata</h2>
								<!-- Title -->
								<h3 class="h3-xl txt-color-05">Theta Healing pentru misiunea de viata si manifestare</h3>
								<!-- Text -->
								<p class="p-lg text-justify">
									La nivel mondial peste 90% din populatie nu este in aliniament cu misiunea de viata, cu scopul personal.
									Misiunea de viata inca se confunda cu jobul si pentru ca cele doua nu sunt simbiozate se creaza si multa nemultumire,
									burnout, tristete, criza. Bineinteles ca se intampla ca la unele persoane misiunea de viata sa fie insasi activitatea profesionala,
									insa daca nu esti convins/a de asta inseamna ca e timpul sa lucram pe acest aspect.
								</p>
								<!-- More Text (initially hidden) -->
								<div id="more-text-4" style="display: none;">
									<p class="p-lg text-justify">
										Sesiunea are ca scop principal echilibrarea energiei, emotiilor, gandurilor personale, pentru ca apoi raspunsurile
										la intrebarile: CINE SUNT? DE CE SUNT AICI? sa isi gaseasca cu usurinta calea.
										Ulterior manifestarea misiunii de viata de zi cu zi sta doar intr-un pas, decizia!
										Te invit sa descoperim povestea misiunii tale de viata, prin eliberarea credintelor limitative si accesarea unei forte interioare.
									</p>
									<p class="p-lg text-justify">
										Iti voi oferi confidentialitate, respect si lipsa de judecat, incat sa accesam in propriul tau ritm adevarul vietii tale.
										Sesiunile se pot sustine atat online, cat si in studioul Yogally din Constanta.
										Tin sa iti multumesc pentru incredere si te felicit in avans pentru decizia de a face pasi spre eliberare.
									</p>
								</div>
								<!-- "Afla mai multe" Link -->
								<p class="p-lg text-justify">
									<a href="#" id="read-more-4">...Afla mai multe</a>
								</p>
							</div>
							<a class="btn btn-md btn-color-02 tra-02-hover privateSesionBtn" aria-label="WhatsApp" href="#contact-form" rel="noopener nofollow" data-subject="Theta Healing pentru misiunea de viata si manifestare">Programeaza-te</a>
						</div>
						<div class="col-xl-5"></div>
					</div>
				</div> <!-- End container -->
			</section> <!-- END Sesiune Privata Theta Healing pentru misiunea de viata si manifestare -->


			<img src="./images/image000683.jpg" alt="" class="w-100 d-xl-none">




			<!-- Sesiune Privata Theta Healing pentru abundenta si manifestare -->
			<section id="services-1" class="abundance bg-color-01 pb-60 services-section division">
				<div class="container">
					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-xl-5"></div>
						<div class="col-lg-7 text-center">
							<div class="section-title mb-60 text-center">
								<!-- Transparent Header -->
								<h2 class="tra-header txt-color-02">Sesiune Privata</h2>
								<!-- Title -->
								<h3 class="h3-xl txt-color-05">Theta Healing pentru abundenta si manifestare</h3>
								<!-- Text -->
								<p class="p-lg text-justify">
									Daca ti-as zice ca saracia este o alegere, ce ai zice?
									Dar daca ti-as zice ca si bogatia este o alegere?
									Cei mai multi clienti ai mei traiau sub falsul adevar care spunea ca “banii sunt murdari”,
									sau “banii sunt rai”, sau “oamenii bogati sunt rai”, sau “cei saraci sunt cei fericiti”.
									COMPLET FALS!
									Prin dreptul la viata pe care l-am castigat in urma nasterii, avem automat si dreptul la abundenta,
									prin insasi taina Sfantului Botez si procesul cufundarii in apa, simbol al unei efuziuni de viata noua si al unei nesecate rodnicii.
								</p>
								<!-- More Text (initially hidden) -->
								<div id="more-text-5" style="display: none;">
									<p class="p-lg text-justify">
										Sesiunea Abundentei este una cu totul speciala si are ca scop conectarea la visul tau cel mai inalt.
										In urma gasirii blocajelor subconstiente ce te tin pe loc in viata de zi cu zi si eliberarea de obstacolele
										preluate de-a lungul vietii tale sau chiar a generatiilor trecute , putem lucra cu usurinta pe manifestarea abundentei.
									</p>
									<p class="p-lg text-justify">
										Iti voi oferi confidentialitate, respect si lipsa de judecat, incat sa accesam in propriul tau ritm adevarul vietii tale.
										Sesiunile se pot sustine atat online, cat si in studioul Yogally din Constanta.
										Tin sa iti multumesc pentru incredere si te felicit in avans pentru decizia de a face pasi spre eliberare.
									</p>
								</div>
								<!-- "Afla mai multe" Link -->
								<p class="p-lg text-justify">
									<a href="#" id="read-more-5">...Afla mai multe</a>
								</p>
							</div>
							<a class="btn btn-md btn-color-02 tra-02-hover privateSesionBtn" aria-label="WhatsApp" href="#contact-form" rel="noopener nofollow" data-subject="Theta Healing pentru abundenta si manifestare">Programeaza-te</a>
						</div>
					</div>
				</div> <!-- End container -->
			</section> <!-- END Sesiune Privata Theta Healing pentru abundenta si manifestare -->


			<img src="./images/image000062.webp" alt="" class="w-100 d-xl-none">




			<!-- Sesiune Privata Theta Healing pentru concepere copil -->
			<section id="services-1" class="child bg-color-01 pb-60 services-section division">
				<div class="container">
					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-lg-7 text-center">
							<div class="section-title mb-60 text-center">
								<!-- Transparent Header -->
								<h2 class="tra-header txt-color-02">Sesiune Privata</h2>
								<!-- Title -->
								<h3 class="h3-xl txt-color-05">Theta Healing pentru concepere copil</h3>
								<!-- Text -->
								<p class="p-lg text-justify">
									In ultimii ani tot mai multe femei, respectiv tot mai multe cupluri nu reusesc sa sustina procesul conceperii unui copil.
									Bineinteles ca sunt si multe cauze externe ce pot determina asta: poluarea, alimentatia, lipsa de somn, burnout, etc.
								</p>
								<!-- More Text (initially hidden) -->
								<div id="more-text-6" style="display: none;">
									<p class="p-lg text-justify">
										In zecile de sesiuni ce le-am avut cu viitoarele mame (uneori chiar si cu partenerii de cuplu) am observat
										ca exista diverse cauze subconstiente ce nu lasa acest vis sa se materializeze.
										La scurt timp de la constientizarea acestor limitari, totul a curs lin si spre binele ambilor parteneri.
									</p>
									<p class="p-lg text-justify">
										Intr-adevar procesul conceperii copilului cat si nasterea si apoi cresterea lui este unul plin de incercari si schimbari,
										pe care multi viitori parinti le cunosc si le accepta; insa sunt multe cauze subconstiente care blocheaza cu totul conceperea fatului.
									</p>
									<p class="p-lg text-justify">
										Daca si tu iti doresti un copil, insa simti ca este ceva ce te blocheaza sau va blocheaza, atunci iti recomand sa programezi o sesiune privata de Theta Healing si concepere copil.
										Vom aborda in ritmul tau personal acele limitari, frici, convingeri care nu lasa visul tau sa se materializeze. Apoi cu responsabilitate si putere personala vei pasi pe drumul conceperii.
									</p>
									<p class="p-lg text-justify">
										Iti voi oferi confidentialitate, respect si lipsa de judecat, incat sa accesam in propriul tau ritm adevarul vietii tale.
										Sesiunile se pot sustine atat online, cat si in studioul Yogally din Constanta.
										Tin sa iti multumesc pentru incredere si te felicit in avans pentru decizia de a face pasi spre eliberare.
									</p>
								</div>
								<!-- "Afla mai multe" Link -->
								<p class="p-lg text-justify">
									<a href="#" id="read-more-6">...Afla mai multe</a>
								</p>
							</div>
							<a class="btn btn-md btn-color-02 tra-02-hover privateSesionBtn" aria-label="WhatsApp" href="#contact-form" rel="noopener nofollow" data-subject="Theta Healing pentru concepere copil">Programeaza-te</a>
						</div>
						<div class="col-xl-5"></div>
					</div>
				</div> <!-- End container -->
			</section> <!-- END Sesiune Privata Theta Healing pentru concepere copil -->


			<img src="./images/image000044.jpg" alt="" class="w-100 d-xl-none">




			<!-- Sesiune Privata Theta Healing pentru eliberarea de boli fizice, emotionale sau de kilogramele in plus -->
			<section id="services-1" class="release bg-color-01 services-section division">
				<div class="container">
					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-xl-5"></div>
						<div class="col-lg-7 text-center">
							<div class="section-title mb-60 text-center">
								<!-- Transparent Header -->
								<h2 class="tra-header txt-color-02">Sesiune Privata</h2>
								<!-- Title -->
								<h3 class="h3-xl txt-color-05">Theta Healing pentru eliberarea de boli fizice, emotionale sau de kilogramele in plus</h3>
								<!-- Text -->
								<p class="p-lg text-justify">
									In ultimii 15 ani tot mai multe persoane tinere sunt “diagnosticate” cu boli cardiovasculare, cu boli de tip diabet, cu alte afectiuni ale sistemului nervos, cu boli autoimune sau chiar kilograme in plus, avand ca si cauza unica STRESUL.
									Este oare stresul cel care determina aceste boli?
								</p>
								<!-- More Text (initially hidden) -->
								<div id="more-text-7" style="display: none;">
									<p class="p-lg text-justify">
										Si DA si NU!
										Stresul poate sa fie o reactie subiectiva a sistemului nervos care ne arata ca lucrurile nu functioneaza in directia mult dorita, este un marker care atunci cand apare ar trebui sa il luam in seama si sa oferim vietii o analiza amanuntita.
										Ceea ce se afla in spatele stresului este de fapt ceea ce determina scaderea imunitatii si in cele din urma materializarea printr-o boala fizica. Cauzele sunt mult mai profunde si chiar subconstiente.
									</p>
									<p class="p-lg text-justify">
										Te invit sa pasim in taramul subconstient al mintii tale care pastreaza tacit emotii si povesti pe care apoi le manifesta in viata de zi cu zi sub forma de boala sau alte afectiuni.
										Vom aborda persoana ta ca un intreg ce contine trup, minte si suflet, intr-un ritm benefic evolutiei tale.
									</p>
									<p class="p-lg text-justify">
										Iti voi oferi confidentialitate, respect si lipsa de judecat, incat sa accesam in propriul tau ritm adevarul vietii tale.
										Sesiunile se pot sustine atat online, cat si in studioul Yogally din Constanta.
										Tin sa iti multumesc pentru incredere si te felicit in avans pentru decizia de a face pasi spre eliberare.
									</p>
								</div>
								<!-- "Afla mai multe" Link -->
								<p class="p-lg text-justify">
									<a href="#" id="read-more-7">...Afla mai multe</a>
								</p>
							</div>
							<a class="btn btn-md btn-color-02 tra-02-hover privateSesionBtn" aria-label="WhatsApp" href="#contact-form" rel="noopener nofollow" data-subject="Theta Healing pentru eliberarea de boli fizice, emotionale sau de kilogramele in plus">Programeaza-te</a>
						</div>
					</div>
				</div> <!-- End container -->
			</section> <!-- END Sesiune Privata Theta Healing pentru eliberarea de boli fizice, emotionale sau de kilogramele in plus -->




			<!-- Yoga
			============================================= -->
			<section id="yoga" class="bg-color-02 services-section division yoga">
				<div class="container">


					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-xl-6">

							<div class="section-title mb-60 text-center">

								<!-- Transparent Header -->
								<h2 class="tra-header txt-color-02">Yoga</h2>

							</div>
							<div class="container">
								<div class="row">


									<!-- TEXT BLOCK -->
									<div class="col-lg-8">
										<div class="txt-block pr-30">

											<!-- Title -->
											<h4 class="h4-xs txt-color-01">Ce este Yoga?</h4>

											<!-- Text -->
											<p class="txt-color-05">
												Yoga este o practica de peste 4000 ani ce isi are originile in India.
												S-a dezvoltat pe parcursul a mii de ani, integrandu-se in diverse traditii religioase
												si filozofice din mai multe regiuni din Asia. Il ultima perioada a cunoscut
												o dezvoltare exploziva in Europa si America, avand milioane de profesori si alte milioane se participanti.
											</p>

											<!-- Text -->
											<p class="txt-color-05">
												Dupa prima interactiune pe care am avut cu Yoga acum 10 ani,
												eu am considerat ca este insasi ea o forma de terapie care lucreaza foarte subtil si benefic cu trupul, mintea si sufletul.
											</p>

											<!-- Title -->
											<h4 class="h4-xs txt-color-01 mt-20">Cu ce ajuta yoga?</h4>

											<!-- Text -->
											<p class="txt-color-05">
												Prin posturile fizice, tehnicile de respiratie, concentrare
												si meditatie reusim sa eliminam blocaje energetice, sa reducem dureri fizice si sa relaxam mintea.
											</p>

										</div>
									</div> <!-- END TEXT BLOCK -->


									<!-- SIDEBAR -->
									<aside id="sidebar" class="col-lg-4">


										<!-- SIDEBAR BOX -->


										<!-- SIDEBAR TABLE -->
										<div class="sidebar-table sidebar-div mb-50">

											<!-- Title -->
											<h5 class="h5-sm txt-color-01">Program</h5>

											<!-- Text -->
											<p class="txt-color-05">
												Sesiunile de Yoga le sustin in orasul in care locuiesc, Constanta, in studioul Yogally situat pe Stefan cel Mare 63 A, etajul 2.
												Fie ca esti incepator sau avansat te invit sa pasesti cu deschidere in casa mea.
											</p>

											<!-- Table -->
											<table class="table txt-color-05">
												<tbody>
													<tr>
														<td>Marti</td>
														<td> - </td>
														<td class="text-right">09:00</td>
													</tr>
													<tr>
														<td>Miercuri</td>
														<td> - </td>
														<td class="text-right">09:00</td>
													</tr>
													<tr class="last-tr">
														<td>Joi</td>
														<td>-</td>
														<td class="text-right">09:00</td>
													</tr>
												</tbody>
											</table>

										</div> <!-- END SIDEBAR TABLE -->


										<!-- IMAGE WIDGET -->
										<div class="image-widget sidebar-div mb-50 text-center">
											<a class="btn btn-md btn-color-02 tra-02-hover privateSesionBtn" aria-label="WhatsApp" href="#contact-form" rel="noopener nofollow">Programeaza-te</a>
										</div>


									</aside> <!-- END SIDEBAR -->


								</div> <!-- End row -->
							</div> <!-- End container -->
						</div>
						<div class="col-xl-6"></div>
					</div>

				</div> <!-- End container -->
			</section> <!-- END Yoga -->

			<img src="./images/image000082.webp" alt="" class="w-100 d-xl-none">



			<!-- Events -->
			<section id="services-1" class="events bg-color-01 pb-60 services-section division">
				<div class="container">
					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-xl-5"></div>
						<div class="col-lg-7 text-center">
							<div class="section-title mb-60 text-center">
								<div class="section-title mb-60 text-center">
									<!-- Transparent Header -->
									<h2 class="tra-header text-light">Evenimente</h2>

								</div>

								<!-- Text -->
								<p class="p-lg text-light">
									Cacao Ceremony, Ritualul Apei, Ritualul Focului, Blind Dance, Citire de carduri Oracol, Baie de sunete, sunt doar cateva din evenimentele deosebite ce le sustin din prea plinul creatiei mele pentru tine.
								</p>
								<p class="p-lg text-light">
									Daca vrei sa stii ca este urmatorul eveniment pe care o sa-l creez
								</p>
							</div>
							<a class="btn btn-md btn-color-02 tra-02-hover privateSesionBtn" aria-label="WhatsApp" href="https://wa.me/+40733104588" target="_blank" rel="noopener nofollow">Aboneaza-te</a>
						</div>
					</div>
				</div> <!-- End container -->
			</section> <!-- END Events -->

			<img src="./images/image000054.webp" alt="" class="w-100 d-xl-none">



			<!-- TESTIMONIALS
			============================================= -->
			<section id="reviews-1" class="bg-color-01 reviews-section division">
				<div class="container">


					<!-- SECTION TITLE -->
					<div class="row">
						<div class="col-lg-10 offset-lg-1">
							<div class="section-title mb-60 text-center">

								<!-- Transparent Header -->
								<h2 class="tra-header txt-color-02">Testimonial</h2>

								<!-- Title 	-->
								<h3 class="h3-xl txt-color-05">Ce spun cursantii</h3>

								<!-- Text -->
								<p class="p-lg txt-color-05">
									Multi dintre cursanti doreau sesiuni private de yoga insa povestile lor aveau nevoie
									de mai multa informatie, cunoastere, asumare si responsabilitate din partea mea.
									Theta Healing<sup>®</sup> a venit de fapt la “cererea” cursantilor mei.
								</p>

							</div>
						</div>
					</div>


					<!-- TESTIMONIALS CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="owl-carousel owl-theme reviews-wrapper">


								<!-- TESTIMONIAL #1 -->
								<div class="review-1">

									<!-- Testimonial Author Avatar -->
									<!-- <div class="testimonial-avatar">
										<img src="images/review-author-1.webp" alt="testimonial-avatar">
									</div> -->

									<!-- Testimonial Author -->
									<div class="author-data txt-color-01">
										<h6 class="h6-sm">Ankh Gaia</h6>
									</div>

									<!-- Testimonial Text -->
									<div class="review-1-txt txt-color-05">
										<p>
											Ședințele Yogally sunt ca o oază de liniște și de revigorare după tumultul cotidian.
											Ușurința îndrumărilor pe care le dă Alexandra face ca yoga să fie o activitate plăcută
											indiferent de nivelul practicii, de la cei mici până la avansați. Ce face însă unic ședințele
											Yogally sunt elementele surpriză prin care Alexandra combină practica yoga cu muzică Live,
											meditații, cacao sau ciocolată caldă, ceea ce dă un sentiment de familiar și de prietenie
											chiar dacă participi pentru prima dată. Recomand Yogally cu mare drag și căldură!
										</p>
									</div>

								</div>


								<!-- TESTIMONIAL #2 -->
								<div class="review-1">

									<!-- Testimonial Author -->
									<div class="author-data txt-color-01">
										<h6 class="h6-sm">Ramona Drutau</h6>
									</div>

									<!-- Testimonial Text -->
									<div class="review-1-txt txt-color-05">
										<p>
											Experiența Yogally este profundă, iar Alexandra este minunată și foarte profi.
											Clasele cu ea sunt o binecuvântare și la sfârșit mă simt ușoară și nu doar fizic,
											ci și psihic. Plec cu o stare de bine și cu o minte limpede pe care rar reușesc să le obțin altfel.
											Recomand din suflet, tuturor o experiență Yogally! Namaste!🙏❤️
										</p>
									</div>

								</div>


								<!-- TESTIMONIAL #3 -->
								<div class="review-1">

									<!-- Testimonial Author -->
									<div class="author-data txt-color-01">
										<h6 class="h6-sm">Nicoleta</h6>
									</div>

									<!-- Testimonial Text -->
									<div class="review-1-txt txt-color-05">
										<p>
											După fiecare ședință cu Alexandra, m-am simțit, în primul rând, foarte relaxată,
											apoi curățată profund de toate rezidurile energetice rămase în corp.
											În perioadele de după ședințe, am observat că lucrurile evoluează frumos în mentalul meu
											și în structura mea energetică. Alexandra este un om calm, vorbește frumos și este foarte
											implicată în ceea ce face! O recomand cu seriozitate și căldură. Sunt bucuroasă că te-am cunoscut. ☀️ 💜💎
										</p>
									</div>

								</div>


								<!-- TESTIMONIAL #4 -->
								<div class="review-1">

									<!-- Testimonial Author -->
									<div class="author-data txt-color-01">
										<h6 class="h6-sm">Andreea Toader</h6>
									</div>

									<!-- Testimonial Text -->
									<div class="review-1-txt txt-color-05">
										<p>
											Locatia este minunata iar ambianta creata de Yogally este de vis.
											Dupa fiecare sesiune parca renasc,fizic ma simt ca un fluturas,
											relaxata si usoara iar psihic simt ca pot muta muntii din loc. Namaste
										</p>
									</div>
								</div>


							</div> <!-- End container -->
			</section> <!-- END TESTIMONIALS -->




			<?php include('contacts.php'); ?>


		</div> <!-- END PAGE CONTENT -->
	</main>

	<?php include('footer.php'); ?>

	<?php include('scripts.php'); ?>


</body>

</html>