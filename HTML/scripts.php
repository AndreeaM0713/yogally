<!-- EXTERNAL SCRIPTS
		============================================= -->

<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.easing.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.scrollto.js"></script>
<script src="js/menu.js"></script>
<script src="js/materialize.js"></script>
<script src="js/tweenmax.min.js"></script>
<script src="js/slideshow.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/hero-form.js"></script>
<script src="js/comment-form.js"></script>
<script src="js/booking-form.js"></script>
<script src="js/jquery.datetimepicker.full.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/gallery.js"></script>

<!-- Custom Script -->
<script src="js/custom.js"></script>
<script src="js/option.js"></script>
<script src="js/pixel.js"></script>