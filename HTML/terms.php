<?php include('head.php'); ?>

<body style="background-color: #f2e9ec">


    <main>
        <!-- PAGE CONTENT
		============================================= -->
        <div id="page" class="page">
            <!-- HEADER
============================================= -->
            <header id="header" class="header tra-menu navbar-light">
                <div class="header-wrapper">


                    <!-- MOBILE HEADER -->
                    <div class="wsmobileheader clearfix">
                        <a href="/">
                            <span class="smllogo"><img src="./images/yogally-logo-black.webp" width="170" alt="mobile-logo" style="margin: 18px;" /></span>
                        </a>
                        <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
                    </div>


                    <!-- NAVIGATION MENU -->
                    <div class="wsmainfull menu clearfix">
                        <div class="wsmainwp clearfix">


                            <!-- LOGO IMAGE -->
                            <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 340 x 100 pixels) -->
                            <div class="desktoplogo"><a href="/" class="logo-black"><img src="./images/yogally-logo-black.webp" height="30" alt="header-logo"></a></div>
                            <div class="desktoplogo"><a href="/" class="logo-white"><img src="./images/yogally-logo.webp" height="30" alt="header-logo"></a></div>


                            <!-- MAIN MENU -->
                            <nav class="wsmenu clearfix">
                                <ul class="wsmenu-list">

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#about">Despre mine</a></li>

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#thetahealing">Theta Healing</a></li>

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#yoga">Yoga & Evenimente</a></li>

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#contact">Contact </a></li>

                                </ul>
                            </nav> <!-- END MAIN MENU -->

                        </div>
                    </div> <!-- END NAVIGATION MENU -->


                </div> <!-- End header-wrapper -->
            </header> <!-- END HEADER -->

            <section class="container" style="padding-top: 7rem;">
                <h1>Termeni si Conditii pentru Yogally</h1>

                <h2>1. Introducere</h2>
                <p>Acesti termeni si conditii guverneaza utilizarea site-ului nostru web. Prin utilizarea site-ului nostru, acceptati acesti termeni si conditii in intregime. Daca nu sunteti de acord cu acesti termeni si conditii sau cu orice parte a acestor termeni si conditii, nu trebuie sa utilizati site-ul nostru web.</p>

                <h2>2. Drepturi de Proprietate Intelectuala</h2>
                <p>Cu exceptia cazului in care se specifica altfel, noi sau licentiatorii nostri detinem drepturile de proprietate intelectuala asupra site-ului si a materialelor de pe site. Sub rezerva licentei de mai jos, toate aceste drepturi de proprietate intelectuala sunt rezervate.</p>

                <h2>3. Licenta de Utilizare a Site-ului</h2>
                <p>Puteti vizualiza, descarca doar in scop de cache si imprima pagini sau alte continuturi de pe site-ul web pentru uz personal, sub rezerva restrictiilor de mai jos si in alte parti din acesti termeni si conditii.</p>
                <p>Nu aveti voie sa:</p>
                <ul>
                    <li>Republicati material de pe acest site web (inclusiv republicarea pe un alt site web);</li>
                    <li>Vindeti, inchiriati sau sub-licentiati materiale de pe site-ul web;</li>
                    <li>Prezentati public orice material de pe site-ul web;</li>
                    <li>Reproduceti, duplicati, copiati sau exploatati materialele de pe site-ul web in scopuri comerciale.</li>
                </ul>

                <h2>4. Utilizare Acceptabila</h2>
                <p>Nu trebuie sa utilizati site-ul nostru web in niciun mod care provoaca sau poate provoca daune site-ului sau deteriorarea accesibilitatii acestuia; sau in orice mod care este ilegal, fraudulos sau daunator, sau in legatura cu orice scop sau activitate ilegala, frauduloasa sau daunatoare.</p>

                <h2>5. Limitari de Raspundere</h2>
                <p>Yogally nu va fi raspunzator fata de dvs. (fie conform legii contactelor, legii delictuale sau altfel) in legatura cu continutul sau utilizarea, sau altfel in legatura cu, acest site web:</p>
                <ul>
                    <li>Pentru orice pierdere indirecta, speciala sau consecventa; sau</li>
                    <li>Pentru orice pierdere de afaceri, pierdere de venituri, venituri, profituri sau economii anticipate, pierdere de contracte sau relatii de afaceri, pierdere de reputatie sau bunavointa, sau pierdere sau coruptie de informatii sau date.</li>
                </ul>

                <h2>6. Excluderi si Limitari</h2>
                <p>Nimic din aceasta excludere de raspundere nu va exclude sau limita orice garantie implicita prin lege ca ar fi ilegal sa fie exclusa sau limitata; si nimic din aceasta excludere de raspundere nu va exclude sau limita raspunderea Yogally in ceea ce priveste:</p>
                <ul>
                    <li>Moartea sau vatamarea personala cauzata de neglijenta Yogally;</li>
                    <li>Frauda sau reprezentarea frauduloasa din partea Yogally;</li>
                    <li>Orice chestiune pe care ar fi ilegal sau nelegal pentru Yogally sa o excluda sau sa o limiteze, sau sa incerce sau sa pretinda sa o excluda sau sa o limiteze, raspunderea sa.</li>
                </ul>

                <h2>7. Modificari</h2>
                <p>Putem revizui acesti termeni si conditii din cand in cand. Termenii si conditiile revizuite se vor aplica utilizarii site-ului nostru web de la data publicarii termenilor si conditiilor revizuite pe site-ul nostru web. Va rugam sa verificati aceasta pagina in mod regulat pentru a va asigura ca sunteti familiarizati cu versiunea actuala.</p>

                <h2>8. Legea Aplicabila si Jurisdictia</h2>
                <p>Acesti termeni si conditii vor fi guvernati si interpretati in conformitate cu legislatia Romaniei, iar orice dispute referitoare la acesti termeni si conditii vor fi supuse jurisdictiei exclusive a instantelor din Romania.</p>

                <h2>9. Informatii de Contact</h2>
                <p>Daca aveti intrebari legate de acesti termeni si conditii, va rugam sa ne contactati la:</p>
                <p>Email: alexandra@yogally.ro<br>
                    Telefon: 0733 104 588<br>

                <h2>Disclaimer</h2>

                <p>Aceste informații sunt oferite în scop informativ și educațional. Site-ul nostru web oferă informații generale despre Theta Healing și yoga și nu constituie sfaturi medicale, legale sau profesionale.</p>

                <p>Niciunul dintre conținuturile site-ului nu este menit să fie un substitut pentru sfaturile oferite de profesioniști calificați în domeniul medical, legal, financiar sau alt domeniu relevant. Utilizarea oricăror informații de pe acest site este pe propriul risc.</p>

                <p>Ne străduim să furnizăm informații precise și actualizate, dar nu garantăm că informațiile sunt complete, corecte sau actuale. Nu suntem responsabili pentru orice daune sau pierderi cauzate direct sau indirect în urma utilizării sau dependenței de conținutul furnizat pe acest site.</p>

                <p>Înainte de a lua orice decizie sau acțiune bazată pe informațiile de pe site-ul nostru, vă recomandăm să căutați sfatul unui profesionist adecvat în domeniul respectiv.</p>

                <p>Pentru întrebări suplimentare sau clarificări, vă rugăm să ne contactați la adresa de email alexandra@yogally.ro sau la numărul de telefon 0733 104 588.</p>
            </section>

        </div> <!-- END PAGE CONTENT -->
    </main>

    <?php include('footer.php'); ?>

    <?php include('scripts.php'); ?>

</body>

</html>