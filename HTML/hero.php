<!-- HERO-4
============================================= -->
<section id="hero-4" class="hero-section division">
    <div class="slideshow" data-swipe="show">
        <div class="slideshow-inner">


            <!-- SLIDER -->
            <div class="slides">


                <!-- SLIDE #1 -->
                <div class="slide is-active first-slide" style="background-image: url('images/image000651.webp'); background-size: cover; background-position: center">

                    <!-- Slide Content -->
                    <div class="slide-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption white-color">

                                        <!-- Title -->
                                        <div class="title" style="text-shadow: 1px 2px 2px #ad858c;">
                                            <h2>Theta Healing</h2>
                                        </div>

                                        <!-- Button -->
                                        <a href="#thetahealing" class="btn btn-md btn-color-02 tra-white-hover">Afla mai multe</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- End Slide Content -->

                    <!-- Slide Background Image -->
                    <!-- <div class="image-container">
                        <img class="img-fluid img-translateX" src="images/image000651.webp" alt="slide-background">
                    </div> -->

                </div> <!-- END SLIDE #1 -->


                <!-- SLIDE #2 -->
                <div class="slide second-slide" style="background-image: url('images/image000023.webp'); background-size: cover; background-position-x: right; background-position-y: bottom;">

                    <!-- Slide Content -->
                    <div class="slide-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption white-color">

                                        <!-- Title -->
                                        <div class="title" style="text-shadow: 1px 2px 2px #ad858c;">
                                            <h2>Yoga</h2>
                                        </div>

                                        <!-- Text -->
                                        <div class="text">

                                            <!-- Button -->
                                            <a href="#yoga" class="btn btn-md btn-color-02 tra-white-hover">Afla mai multe</a>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- End Slide Content -->

                    <!-- Slide Background Image -->
                    <!-- <div class="image-container">
                        <img class="img-fluid img-translateX" src="images/image000023.webp" alt="slide-background">
                    </div> -->

                </div> <!-- END SLIDE #2 -->


                <!-- SLIDE #3 -->
                <div class="slide third-slide" style="background-image: url('images/image00003.webp'); background-size: cover; background-position: center;">

                    <!-- Slide Content -->
                    <div class="slide-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="caption text-center white-color">

                                        <!-- Title -->
                                        <div class="title" style="text-shadow: 1px 2px 2px #ad858c;">
                                            <h2>Workshopuri</h2>
                                        </div>

                                        <!-- Text -->
                                        <div class="text">



                                            <!-- Button -->
                                            <a href="#events" class="btn btn-md btn-color-02 tra-white-hover">Afla mai multe</a>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- End Slide Content -->

                    <!-- Slide Background Image -->
                    <!-- <div class="image-container">
                        <img class="img-fluid img-translateX" src="images/image00003.webp" alt="slide-background">
                    </div> -->

                </div> <!-- END SLIDE #3 -->


            </div> <!-- END SLIDER -->


            <!-- SLIDER ARROWS -->
            <div class="arrows ico-45 white-color">
                <div class="arrow prev"><span class="flaticon-back"> </span></div>
                <div class="arrow next"><span class="flaticon-next"> </span></div>
            </div>


        </div> <!-- End Slideshow Inner -->
    </div> <!-- End Slideshow -->
</section> <!-- END HERO-4 -->