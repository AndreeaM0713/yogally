<?php include('head.php'); ?>

<body style="background-color: #f2e9ec">


    <main>
        <!-- PAGE CONTENT
		============================================= -->
        <div id="page" class="page">
            <!-- HEADER
============================================= -->
            <header id="header" class="header tra-menu navbar-light">
                <div class="header-wrapper">


                    <!-- MOBILE HEADER -->
                    <div class="wsmobileheader clearfix">
                        <a href="#">
                            <span class="smllogo"><img src="./images/yogally-logo-black.webp" width="170" alt="mobile-logo" style="margin: 18px;" /></span>
                        </a>
                        <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
                    </div>


                    <!-- NAVIGATION MENU -->
                    <div class="wsmainfull menu clearfix">
                        <div class="wsmainwp clearfix">


                            <!-- LOGO IMAGE -->
                            <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 340 x 100 pixels) -->
                            <div class="desktoplogo"><a href="/" class="logo-black"><img src="./images/yogally-logo-black.webp" height="30" alt="header-logo"></a></div>
                            <div class="desktoplogo"><a href="/" class="logo-white"><img src="./images/yogally-logo.webp" height="30" alt="header-logo"></a></div>


                            <!-- MAIN MENU -->
                            <nav class="wsmenu clearfix">
                                <ul class="wsmenu-list">

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#about">Despre mine</a></li>

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#thetahealing">Theta Healing</a></li>

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#yoga">Yoga & Evenimente</a></li>

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#contact">Contact </a></li>

                                </ul>
                            </nav> <!-- END MAIN MENU -->

                        </div>
                    </div> <!-- END NAVIGATION MENU -->


                </div> <!-- End header-wrapper -->
            </header> <!-- END HEADER -->

            <section class="container" style="padding-top: 7rem;">
                <h1>Politica de Confidentialitate</h1>

                <p>Ultima actualizare: 01 iulie 2024</p>

                <h2>1. Colectarea Informatiilor Personale</h2>
                <p>Colectam informatii personale pe acest site web atunci cand ne contactati prin formularul de contact sau cand alegeti sa va inscrieti la cursurile noastre de Theta Healing si yoga. Informatiile pe care le colectam pot include nume, adresa de email, numar de telefon si alte detalii relevante pentru a va oferi serviciile noastre.</p>

                <h2>2. Utilizarea Informatiilor Colectate</h2>
                <p>Informatiile personale pe care le colectam sunt utilizate in principal pentru a va oferi serviciile solicitate si pentru a va gestiona contul. De asemenea, utilizam informatiile pentru a va contacta cu privire la intrebari, rezervari si actualizari ale serviciilor noastre.</p>

                <h2>3. Protejarea Informatiilor</h2>
                <p>Ne angajam sa protejam informatiile personale pe care le colectam si sa utilizam masuri de securitate adecvate pentru a preveni accesul neautorizat sau divulgarea acestora.</p>

                <h2>4. Partajarea Informatiilor cu Terte Parti</h2>
                <p>Nu vom partaja informatiile dvs. personale cu terte parti fara consimtamantul dvs., cu exceptia cazurilor in care este necesar sa indeplinim cerintele legale.</p>

                <h2>5. Drepturile Dvs.</h2>
                <p>in conformitate cu legislatia privind protectia datelor, aveti dreptul de a accesa, corecta, actualiza sau sterge informatiile personale pe care le detinem. De asemenea, aveti dreptul de a restrictiona sau de a opune procesarii acestor informatii. Pentru orice solicitare legata de drepturile dvs. de confidentialitate, va rugam sa ne contactati folosind informatiile de contact de mai jos.</p>

                <h2>6. Modificari ale Politicii de Confidentialitate</h2>
                <p>Ne rezervam dreptul de a actualiza sau modifica aceasta politica de confidentialitate in orice moment. Orice modificari vor fi publicate pe aceasta pagina, cu o actualizare a datei de intrare in vigoare de mai sus.</p>

                <h2>7. Contact</h2>
                <p>Pentru intrebari suplimentare despre politica noastra de confidentialitate, va rugam sa ne contactati la:</p>
                <p>Email: alexandra@yogally.ro<br>
                    Telefon: 0733 104 588<br>
            </section>

        </div> <!-- END PAGE CONTENT -->
    </main>

    <?php include('footer.php'); ?>

    <?php include('scripts.php'); ?>

</body>

</html>