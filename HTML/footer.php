<!-- FOOTER-4
    ============================================= -->
<footer id="footer-4" class="bg-color-01 footer division">
    <div class="container">


        <!-- FOOTER CONTENT -->
        <div class="row">


            <!-- FOOTER INFO -->
            <div class="col-md-5 col-lg-4">
                <div class="footer-info mb-40">

                    <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be 
                        displayed (e.g 408 x 120  pixels) -->
                    <img src="images/yogally-logo-orange.webp" width="204" alt="footer-logo">

                    <!-- Text -->
                    <p class="txt-color-05 mt-20 text-justify" style="width: 60%;">
                        "Peace comes from within.
                        Do not seek it without"
                    </p>
                    <p class="txt-color-05 mt-20 font-italic">
                        Buddha
                    </p>

                </div>
            </div>


            <!-- FOOTER CONTACTS -->
            <div class="col-md-4 col-lg-3 col-xl-3">
                <div class="footer-contacts mb-40">

                    <!-- Title -->
                    <h6 class="h6-lg txt-color-01">Hai sa discutam</h6>

                    <!-- Footer Contacts -->
                    <div class="txt-color-05 mt-15">

                        <!-- Email -->
                        <p class="small-text"><i class="fas fa-envelope"></i><a href="mailto:alexandra@yogally.ro"> alexandra@yogally.ro </a> </p>

                        <!-- Phone -->
                        <p class="small-text"><i class="fas fa-phone"></i><a href="tel:+40733104588"> 0733 104 588 </a> </p>

                        <!-- WhatsApp -->
                        <p class="small-text"><i class="fab fa-whatsapp"></i> 0733 104 588 </p>

                    </div>

                </div>
            </div>


            <!-- FOOTER LINKS -->
            <div class="col-md-3 col-lg-2">
                <div class="footer-links mb-40">

                    <!-- Title -->
                    <h6 class="h6-lg txt-color-01">Linkuri rapide</h6>

                    <!-- Footer Links -->
                    <ul class="txt-color-05 clearfix">
                        <li>
                            <p><a href="#about">Despre mine</a></p>
                        </li>
                        <li>
                            <p><a href="#thetahealing">Theta Healing</a></p>
                        </li>
                        <li>
                            <p><a href="#yoga">Yoga & Evenimente</a></p>
                        </li>
                        <li>
                            <p><a href="#contact">Contact</a></p>
                        </li>
                    </ul>

                </div>
            </div>


            <!-- FOOTER IMAGES -->
            <div class="col-md-12 col-lg-3">
                <div class="footer-img mb-40">

                    <!-- Title -->
                    <h6 class="h6-lg txt-color-01">Instagram</h6>

                    <!-- Instagram Images -->
                    <ul class="text-center clearfix">
                        <li><a href="https://www.instagram.com/yogally_alexandra/" target="_blank"><img class="insta-img" src="images/image00001_1x1.webp" alt="insta-img"></a></li>
                        <li><a href="https://www.instagram.com/yogally_alexandra/" target="_blank"><img class="insta-img" src="images/image00005_1x1.webp" alt="insta-img"></a></li>
                        <li><a href="https://www.instagram.com/yogally_alexandra/" target="_blank"><img class="insta-img" src="images/image00003_1x1.webp" alt="insta-img"></a></li>
                        <li><a href="https://www.instagram.com/yogally_alexandra/" target="_blank"><img class="insta-img" src="images/image00004_1x1.webp" alt="insta-img"></a></li>
                        <li><a href="https://www.instagram.com/yogally_alexandra/" target="_blank"><img class="insta-img" src="images/image00010_1x1.webp" alt="insta-img"></a></li>
                        <li><a href="https://www.instagram.com/yogally_alexandra/" target="_blank"><img class="insta-img" src="images/image00004_1x1.webp" alt="insta-img"></a></li>
                    </ul>

                </div>
            </div> <!-- END FOOTER IMAGES -->


        </div> <!-- END FOOTER CONTENT -->


        <!-- BOTTOM FOOTER -->
        <div class="bottom-footer txt-color-05">
            <div class="row d-flex align-items-center">


                <!-- FOOTER COPYRIGHT -->
                <div class="col-lg-4">
                    <div class="footer-copyright">
                        <p>&copy; 2024 Yogally. All Rights Reserved</p>
                    </div>
                </div>

                <!-- BOTTOM FOOTER TERMS -->
                <div class="col-lg-6">
                    <ul class="clearfix legal" style="font-size: smaller;">
                        <li>
                            <p class="first-list-link"><a href="terms.php">Termeni si conditii</a></p>
                        </li>
                        <li>
                            <p><a href="confidence-policy.php">Politica de confidentialitate</a></p>
                        </li>
                        <li>
                            <p><a href="cookies.php">Politica de utilizare Cookie-uri</a></p>
                        </li>
                    </ul>
                </div>

                <!-- BOTTOM FOOTER LINKS -->
                <div class="col-lg-2">
                    <ul class="bottom-footer-list text-right clearfix">
                        <li>
                            <p class="first-list-link"><a href="https://www.facebook.com/yogally26/" target="_blank"><i class="fab fa-facebook-f"></i></a></p>
                        </li>
                        <li>
                            <p><a href="https://www.instagram.com/yogally_alexandra/" target="_blank"><i class="fab fa-instagram"></i></a></p>
                        </li>
                        <li>
                            <p><a href="https://on.SoundCloud.com/gpFqtV9qfiBM4EN88" target="_blank"><i class="fab fa-soundcloud"></i></a></p>
                        </li>
                    </ul>
                </div>


            </div> <!-- End row -->
        </div> <!-- END BOTTOM FOOTER -->


    </div> <!-- End container -->
</footer> <!-- END FOOTER-4 -->