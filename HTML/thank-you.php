<?php include('head.php'); ?>

<body>


    <main>
        <!-- PAGE CONTENT
		============================================= -->
        <div id="page" class="page">
            <?php include('header.php'); ?>


            <section class="thank-you division about-container-left">
                <div class="thank-you-text">
                    <h2 class="tra-header txt-color-02">Multumesc!</h2>
                    <div class="text-center">
                        <h3 class="h3-xl txt-color-05">Mesajul tau <br> este pe drum</h3>
                        <p class="p-lg txt-color-05">Iti voi raspunde in scurt timp!</p>
                    </div>
                </div>
            </section>

        </div> <!-- END PAGE CONTENT -->
    </main>

    <?php include('footer.php'); ?>

    <?php include('scripts.php'); ?>


</body>

</html>