<?php include('head.php'); ?>

<body style="background-color: #f2e9ec">


    <main>
        <!-- PAGE CONTENT
		============================================= -->
        <div id="page" class="page">
            <!-- HEADER
============================================= -->
            <header id="header" class="header tra-menu navbar-light">
                <div class="header-wrapper">


                    <!-- MOBILE HEADER -->
                    <div class="wsmobileheader clearfix">
                        <a href="/">
                            <span class="smllogo"><img src="./images/yogally-logo-black.webp" width="170" alt="mobile-logo" style="margin: 18px;" /></span>
                        </a>
                        <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
                    </div>


                    <!-- NAVIGATION MENU -->
                    <div class="wsmainfull menu clearfix">
                        <div class="wsmainwp clearfix">


                            <!-- LOGO IMAGE -->
                            <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 340 x 100 pixels) -->
                            <div class="desktoplogo"><a href="/" class="logo-black"><img src="./images/yogally-logo-black.webp" height="30" alt="header-logo"></a></div>
                            <div class="desktoplogo"><a href="/" class="logo-white"><img src="./images/yogally-logo.webp" height="30" alt="header-logo"></a></div>


                            <!-- MAIN MENU -->
                            <nav class="wsmenu clearfix">
                                <ul class="wsmenu-list">

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#about">Despre mine</a></li>

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#thetahealing">Theta Healing</a></li>

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#yoga">Yoga & Evenimente</a></li>

                                    <!-- SIMPLE NAVIGATION LINK -->
                                    <li><a href="/#contact">Contact </a></li>

                                </ul>
                            </nav> <!-- END MAIN MENU -->

                        </div>
                    </div> <!-- END NAVIGATION MENU -->


                </div> <!-- End header-wrapper -->
            </header> <!-- END HEADER -->

            <section class="container" style="padding-top: 7rem;">
                <h1>Politica de Cookie-uri</h1>

                <p>Ultima actualizare: 01 iulie 2024</p>

                <h2>1. Ce sunt cookie-urile?</h2>
                <p>Cookie-urile sunt fișiere mici de text stocate pe dispozitivul dumneavoastră atunci când vizitați un site web. Ele sunt utilizate pentru a îmbunătăți experiența utilizatorului, pentru a furniza funcționalități esențiale și pentru a colecta informații despre utilizarea site-ului.</p>

                <h2>2. Tipuri de Cookie-uri pe care le Folosim</h2>
                <ul>
                    <li><strong>Cookie-uri necesare:</strong> Aceste cookie-uri sunt esențiale pentru funcționarea site-ului nostru și nu pot fi dezactivate în sistemele noastre. Ele sunt de obicei setate doar ca răspuns la acțiuni făcute de dumneavoastră, cum ar fi setarea preferințelor de confidențialitate, autentificarea sau completarea formularelor.</li>
                    <li><strong>Cookie-uri de performanță:</strong> Aceste cookie-uri colectează informații anonime despre modul în care vizitatorii utilizează site-ul nostru, cum ar fi paginile pe care le accesează cel mai des. Aceste informații ne ajută să îmbunătățim modul în care funcționează site-ul nostru.</li>
                    <li><strong>Cookie-uri funcționale:</strong> Aceste cookie-uri permit site-ului nostru să ofere funcționalități îmbunătățite și personalizate, cum ar fi reținerea preferințelor dumneavoastră (de exemplu, limba sau regiunea). Ele pot fi setate de noi sau de terțe părți ale căror servicii le-am adăugat pe paginile noastre.</li>
                    <li><strong>Cookie-uri de targetare:</strong> Aceste cookie-uri pot fi setate prin site-ul nostru de partenerii noștri de publicitate. Ele pot fi utilizate de aceste companii pentru a crea un profil al intereselor dumneavoastră și pentru a vă arăta reclame relevante pe alte site-uri.</li>
                </ul>

                <h2>3. Cum Gestionați Cookie-urile</h2>
                <p>Puteți seta browser-ul dumneavoastră să blocheze sau să vă alerteze despre aceste cookie-uri, dar unele părți ale site-ului ar putea să nu funcționeze corect. Majoritatea browser-elor web permit un anumit control asupra cookie-urilor prin setările browser-ului. Pentru a afla mai multe despre cookie-uri, inclusiv despre cum să vedeți ce cookie-uri au fost setate și cum să le gestionați și să le ștergeți, vizitați <a href="http://www.allaboutcookies.org" target="_blank">www.allaboutcookies.org</a>.</p>

                <h2>4. Modificări ale Politicii de Cookie-uri</h2>
                <p>Putem actualiza această politică de cookie-uri din când în când pentru a reflecta modificările aduse practicilor noastre de utilizare a cookie-urilor. Orice modificări vor fi publicate pe această pagină și vor intra în vigoare imediat după publicare.</p>

                <h2>5. Contact</h2>
                <p>Dacă aveți întrebări sau preocupări legate de utilizarea cookie-urilor pe site-ul nostru, vă rugăm să ne contactați la:</p>
                <p>Email: alexandra@yogally.ro<br>
                    Telefon: 0733 104 588<br>
            </section>

        </div> <!-- END PAGE CONTENT -->
    </main>

    <?php include('footer.php'); ?>

    <?php include('scripts.php'); ?>

</body>