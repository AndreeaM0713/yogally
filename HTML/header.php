<!-- HEADER
============================================= -->
<header id="header" class="header tra-menu navbar-light">
    <div class="header-wrapper">


        <!-- MOBILE HEADER -->
        <div class="wsmobileheader clearfix">
            <a href="#">
                <span class="smllogo"><img src="./images/yogally-logo-black.webp" width="170" alt="mobile-logo" style="margin: 18px;"/></span>
            </a>
            <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
        </div>


        <!-- NAVIGATION MENU -->
        <div class="wsmainfull menu clearfix">
            <div class="wsmainwp clearfix">


                <!-- LOGO IMAGE -->
                <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 340 x 100 pixels) -->
                <div class="desktoplogo"><a href="/" class="logo-black"><img src="./images/yogally-logo-black.webp" height="30" alt="header-logo"></a></div>
                <div class="desktoplogo"><a href="/" class="logo-white"><img src="./images/yogally-logo.webp" height="30" alt="header-logo"></a></div>


                <!-- MAIN MENU -->
                <nav class="wsmenu clearfix">
                    <ul class="wsmenu-list">

                        <!-- SIMPLE NAVIGATION LINK -->
                        <li><a href="/#about">Despre mine</a></li>

                        <!-- SIMPLE NAVIGATION LINK -->
                        <li><a href="/#thetahealing">Theta Healing</a></li>

                        <!-- SIMPLE NAVIGATION LINK -->
                        <li><a href="/#yoga">Yoga & Evenimente</a></li>

                        <!-- SIMPLE NAVIGATION LINK -->
                        <li><a href="/#contact">Contact </a></li>

                    </ul>
                </nav> <!-- END MAIN MENU -->

            </div>
        </div> <!-- END NAVIGATION MENU -->


    </div> <!-- End header-wrapper -->
</header> <!-- END HEADER -->