<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $secretKey = "6LdwqBEqAAAAAGsM66hXwyJDYNk_5H-IYdOucNHi";
    $clientCaptchaResponse = $_POST['g-recaptcha-response'];
    $userIp = $_SERVER['REMOTE_ADDR'];
    $recaptchaUrl = "https://www.google.com/recaptcha/api/siteverify";
    $ch = curl_init($recaptchaUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, [
        'secret' => $secretKey,
        'response' => $clientCaptchaResponse,
        'remoteip' => $userIp
    ]);
    $recaptchaResponse = curl_exec($ch);
    curl_close($ch);
    $recaptchaResponseDecoded = json_decode($recaptchaResponse);
    if (!$recaptchaResponseDecoded->success) {
        echo json_encode(['success' => false, 'message' => 'Ai picat testul reCAPTCHA. CSF NAI CSF']);
        exit();
    }
    $toEmail = "andreea.mitroi28@gmail.com";
    $subject = "Mesaj nou @ Yogalily de la " . $_POST['userName'];
    $mailHeaders = "From: " . $_POST['userEmail'] . "\r\n";
    $mailHeaders .= "Name: " . $_POST['userName'] . "\r\n";
    $mailHeaders .= "Phone: " . $_POST['userPhone'] . "\r\n";
    $mailHeaders .= "Subject: " . $_POST['userSubject'] . "\r\n";
    $mailHeaders .= "Message: " . $_POST['userMessage'] . "\r\n";

    if (mail($toEmail, $subject, $mailHeaders)) {
        echo json_encode(['success' => true]);
    } else {
        echo json_encode(['success' => false, 'message' => 'Nu s-a putut trimite mesajul, incearca din nou.']);
    }
}
?>

<section>
    <div id="page" class="page">
        <div class="inner-page-wrapper">
            <section id="contact" class="bg-color-02 contacts-section division">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                            <div class="section-title mb-60 text-center">
                                <h2 class="tra-header txt-color-02">Contacteaza-ne</h2>
                                <h3 class="h3-xl txt-color-05">Ai intrebari?</h3>
                                <p class="p-lg txt-color-05">
                                    Iti stau la dispozitie pentru eventualele curiozitati.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="google-map mb-80">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2861.454621739347!2d28.64311657665093!3d44.17709921838877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40bae530acc08497%3A0xd40ea9c63220b699!2sStrada%20%C8%98tefan%20cel%20Mare%2063%2C%20Constan%C8%9Ba%20900178!5e0!3m2!1sen!2sro!4v1718094121504!5m2!1sen!2sro"
                                    width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-7 col-lg-8" id="contact-form">
                            <h4 class="h4-xs txt-color-01">Hai sa ne cunoastem, completeaza cateva detalii si trimite-mi un mesaj.</h4>
                            <div class="form-holder">
                            <form name="contacts" class="row contact-form" id="contact-form-captcha" method="POST" onsubmit="validateForm(event)">
                                    <div class="col-lg-4">
                                        <input id="userName" type="text" name="userName" class="form-control" placeholder="Prenume" required>
                                    </div>
                                    <div class="col-lg-4">
                                        <input id="userPhone" type="tel" name="userPhone" class="form-control" placeholder="Telefon" required>
                                    </div>
                                    <div class="col-lg-4">
                                        <input id="userEmail" type="email" name="userEmail" class="form-control" placeholder="Email*" required>
                                    </div>
                                    <div class="col-md-12">
                                        <select id="userSubject" name="userSubject" class="form-control" required>
                                            <option class="subjectOption" value="" disabled selected>Selecteaza subiect</option>
                                            <option class="subjectOption" value="Theta Healing pentru Anxietate, Atacuri de panica, Depresie, Insomnie">Theta Healing pentru Anxietate, Atacuri de panica, Depresie, Insomnie</option>
                                            <option class="subjectOption" value="Theta Healing si Divortul Energetic">Theta Healing si Divortul Energetic</option>
                                            <option class="subjectOption" value="Theta Healing si Atragerea sufletului pereche potrivit si optim">Theta Healing si Atragerea sufletului pereche potrivit si optim</option>
                                            <option class="subjectOption" value="Theta Healing pentru misiunea de viata si manifestare">Theta Healing pentru misiunea de viata si manifestare</option>
                                            <option class="subjectOption" value="Theta Healing pentru abundenta si manifestare">Theta Healing pentru abundenta si manifestare</option>
                                            <option class="subjectOption" value="Theta Healing pentru concepere copil">Theta Healing pentru concepere copil</option>
                                            <option class="subjectOption" value="Theta Healing pentru eliberarea de boli fizice, emotionale sau de kilogramele in plus">Theta Healing pentru eliberarea de boli fizice, emotionale sau de kilogramele in plus</option>
                                            <option class="subjectOption" value="Altele">Altele</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea id="userMessage" name="userMessage" class="form-control" rows="6" placeholder="Imi poti lasa cateva cuvinte aici ..." required></textarea>
                                    </div>
                                    <div class="col-md-12" >
                                        <div id="error_message" class="alert alert-danger d-none">A intervenit o eroare, incearca mai tarziu...</div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="checkbox" id="agree" name="agree" required />
                                        <label for="agree">Sunt de acord cu <a href="#">termenii GDPR</a></label>
                                    </div>
                                    <div class="g-recaptcha" style="margin-left:13px;" data-sitekey="6LdwqBEqAAAAAOzJhv1SJkl3I13cdWBiZW04-NSM"></div>
                                    <div class="col-md-12 mt-5 pb-4 text-left">
                                        <button type="submit" class="g-recaptcha btn btn-md btn-color-02 tra-02-hover">Trimite Mesaj</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-5 col-lg-4">
                            <div class="contacts-info mb-40">
                                <div class="cbox-1 mb-25">
                                    <h5 class="h5-xs txt-color-01">Pentru programari sau detalii ma poti suna.</h5>
                                    <p class="small-text"><i class="fas fa-phone"></i><a href="tel:+40733104588"> 0733 104 588 </a></p>
                                </div>

                                <div class="cbox-1 mb-25">
                                    <h5 class="h5-xs txt-color-01">Imi poti lasa mesaj pe WhatsApp sau pe e-mail</h5>
                                    <p class="small-text"><i class="fab fa-whatsapp"></i> 0733 104 588 </p>
                                    <p class="small-text"><i class="fas fa-envelope"></i><a href="mailto:alexandra@yogally.ro"> alexandra@yogally.ro </a></p>
                                </div>

                                <div class="cbox-1 mt-25 mb-25">
                                    <h5 class="h5-xs txt-color-01">Locatie</h5>
                                    <p class="txt-color-05">Studio Yogally</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script>
        function validateForm(event) {
            var response = grecaptcha.getResponse();
            if (response.length === 0) {
                event.preventDefault();
                alert('Completeaza reCAPTCHA pentru a continua.');
                return;
            }
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "contacts.php",
                data: $(".contact-form").serialize(),
                success: function(response) {
                    try {
                        const jsonResponse = JSON.parse(response);
                        if (jsonResponse.success) {
                            $(".contact-form")[0].reset();
                            document.getElementById("error_message").classList.add("d-none");
                            window.location.href = "thank-you.php";
                        } else {
                            $("#error_message").removeClass("d-none").text(jsonResponse.message);
                        }
                    } catch (e) {
                        document.getElementById("error_message").classList.remove("d-none");
                    }
                },
                error: function() {
                    document.getElementById("error_message").classList.remove("d-none");
                }
            });
        }
    </script>