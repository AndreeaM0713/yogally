document.addEventListener('DOMContentLoaded', function () {

  var moreText = document.getElementById('more-text');
  var readMoreLink = document.getElementById('read-more');

  readMoreLink.addEventListener('click', function (e) {
    e.preventDefault();
    if (moreText.style.display === 'none') {
      moreText.style.display = 'block';
      readMoreLink.style.display = 'none';
    }
  });

  var moreText2 = document.getElementById('more-text-2');
  var readMoreLink2 = document.getElementById('read-more-2');

  readMoreLink2.addEventListener('click', function (e) {
    e.preventDefault();
    if (moreText2.style.display === 'none') {
      moreText2.style.display = 'block';
      readMoreLink2.style.display = 'none';
    }
  });

  var moreText3 = document.getElementById('more-text-3');
  var readMoreLink3 = document.getElementById('read-more-3');

  readMoreLink3.addEventListener('click', function (e) {
    e.preventDefault();
    if (moreText3.style.display === 'none') {
      moreText3.style.display = 'block';
      readMoreLink3.style.display = 'none';
    }
  });

  var moreText4 = document.getElementById('more-text-4');
  var readMoreLink4 = document.getElementById('read-more-4');

  readMoreLink4.addEventListener('click', function (e) {
    e.preventDefault();
    if (moreText4.style.display === 'none') {
      moreText4.style.display = 'block';
      readMoreLink4.style.display = 'none';
    }
  });

  var moreText5 = document.getElementById('more-text-5');
  var readMoreLink5 = document.getElementById('read-more-5');

  readMoreLink5.addEventListener('click', function (e) {
    e.preventDefault();
    if (moreText5.style.display === 'none') {
      moreText5.style.display = 'block';
      readMoreLink5.style.display = 'none';
    }
  });

  var moreText6 = document.getElementById('more-text-6');
  var readMoreLink6 = document.getElementById('read-more-6');

  readMoreLink6.addEventListener('click', function (e) {
    e.preventDefault();
    if (moreText6.style.display === 'none') {
      moreText6.style.display = 'block';
      readMoreLink6.style.display = 'none';
    }
  });

  var moreText7 = document.getElementById('more-text-7');
  var readMoreLink7 = document.getElementById('read-more-7');

  readMoreLink7.addEventListener('click', function (e) {
    e.preventDefault();
    if (moreText7.style.display === 'none') {
      moreText7.style.display = 'block';
      readMoreLink7.style.display = 'none';
    }
  });

  var slideshow = document.querySelector('.slideshow');
  var slides = document.querySelectorAll('.slide');
  var startX, startY, endX, endY;

  function handleSwipe() {
    var deltaX = endX - startX;
    var deltaY = endY - startY;
    if (Math.abs(deltaX) > Math.abs(deltaY)) {
      if (deltaX > 50) {
        // Swipe right
        slideshowNext($(slideshow), true, true);
      } else if (deltaX < -50) {
        // Swipe left
        slideshowNext($(slideshow), false, true);
      }
    }
  }

  slideshow.addEventListener('touchstart', function (e) {
    var touch = e.touches[0];
    startX = touch.clientX;
    startY = touch.clientY;
  });

  slideshow.addEventListener('touchmove', function (e) {
    var touch = e.touches[0];
    endX = touch.clientX;
    endY = touch.clientY;
  });

  slideshow.addEventListener('touchend', function (e) {
    handleSwipe();
  });

  var slideshowDuration = 4000;
  var slideshow = $('.slideshow');

  function slideshowSwitch(slideshow, index, auto) {
    if (slideshow.data('wait')) return;

    var slides = slideshow.find('.slide');
    var pages = slideshow.find('.pagination');
    var activeSlide = slides.filter('.is-active');
    var activeSlideImage = activeSlide.find('.image-container');
    var newSlide = slides.eq(index);
    var newSlideImage = newSlide.find('.image-container');
    var newSlideContent = newSlide.find('.slide-content');
    var newSlideElements = newSlide.find('.caption > *');
    if (newSlide.is(activeSlide)) return;

    newSlide.addClass('is-new');
    var timeout = slideshow.data('timeout');
    clearTimeout(timeout);
    slideshow.data('wait', true);
    var transition = slideshow.attr('data-transition');
    if (transition == 'fade') {
      newSlide.css({
        display: 'block',
        zIndex: 2
      });
      newSlideImage.css({
        opacity: 0
      });

      TweenMax.to(newSlideImage, 1, {
        alpha: 1,
        onComplete: function () {
          newSlide.addClass('is-active').removeClass('is-new');
          activeSlide.removeClass('is-active');
          newSlide.css({
            display: '',
            zIndex: ''
          });
          newSlideImage.css({
            opacity: ''
          });
          slideshow.find('.pagination').trigger('check');
          slideshow.data('wait', false);
          if (auto) {
            timeout = setTimeout(function () {
              slideshowNext(slideshow, false, true);
            }, slideshowDuration);
            slideshow.data('timeout', timeout);
          }
        }
      });
    } else {
      if (newSlide.index() > activeSlide.index()) {
        var newSlideRight = 0;
        var newSlideLeft = 'auto';
        var newSlideImageRight = -slideshow.width() / 8;
        var newSlideImageLeft = 'auto';
        var newSlideImageToRight = 0;
        var newSlideImageToLeft = 'auto';
        var newSlideContentLeft = 'auto';
        var newSlideContentRight = 0;
        var activeSlideImageLeft = -slideshow.width() / 4;
      } else {
        var newSlideRight = '';
        var newSlideLeft = 0;
        var newSlideImageRight = 'auto';
        var newSlideImageLeft = -slideshow.width() / 8;
        var newSlideImageToRight = '';
        var newSlideImageToLeft = 0;
        var newSlideContentLeft = 0;
        var newSlideContentRight = 'auto';
        var activeSlideImageLeft = slideshow.width() / 4;
      }

      newSlide.css({
        display: 'block',
        width: 0,
        right: newSlideRight,
        left: newSlideLeft,
        zIndex: 2
      });

      newSlideImage.css({
        width: slideshow.width(),
        right: newSlideImageRight,
        left: newSlideImageLeft
      });

      newSlideContent.css({
        width: slideshow.width(),
        left: newSlideContentLeft,
        right: newSlideContentRight
      });

      activeSlideImage.css({
        left: 0
      });

      TweenMax.set(newSlideElements, { y: 20, force3D: true });
      TweenMax.to(activeSlideImage, 1, {
        left: activeSlideImageLeft,
        ease: Power3.easeInOut
      });

      TweenMax.to(newSlide, 1, {
        width: slideshow.width(),
        ease: Power3.easeInOut
      });

      TweenMax.to(newSlideImage, 1, {
        right: newSlideImageToRight,
        left: newSlideImageToLeft,
        ease: Power3.easeInOut
      });

      TweenMax.staggerFromTo(newSlideElements, 0.8, { alpha: 0, y: 60 }, { alpha: 1, y: 0, ease: Power3.easeOut, force3D: true, delay: 0.6 }, 0.1, function () {
        newSlide.addClass('is-active').removeClass('is-new');
        activeSlide.removeClass('is-active');
        newSlide.css({
          display: '',
          width: '',
          left: '',
          zIndex: ''
        });

        newSlideImage.css({
          width: '',
          right: '',
          left: ''
        });

        newSlideContent.css({
          width: '',
          left: ''
        });

        newSlideElements.css({
          opacity: '',
          transform: ''
        });

        activeSlideImage.css({
          left: ''
        });

        slideshow.find('.pagination').trigger('check');
        slideshow.data('wait', false);
        if (auto) {
          timeout = setTimeout(function () {
            slideshowNext(slideshow, false, true);
          }, slideshowDuration);
          slideshow.data('timeout', timeout);
        }
      });
    }
  }

  function slideshowNext(slideshow, previous, auto) {
    var slides = slideshow.find('.slide');
    var activeSlide = slides.filter('.is-active');
    var newSlide = null;
    if (previous) {
      newSlide = activeSlide.prev('.slide');
      if (newSlide.length === 0) {
        newSlide = slides.last();
      }
    } else {
      newSlide = activeSlide.next('.slide');
      if (newSlide.length == 0)
        newSlide = slides.filter('.slide').first();
    }

    slideshowSwitch(slideshow, newSlide.index(), auto);
  }

  function homeSlideshowParallax() {
    var scrollTop = $(window).scrollTop();
    if (scrollTop > windowHeight) return;
    var inner = slideshow.find('.slideshow-inner');
    var newHeight = windowHeight - (scrollTop / 2);
    var newTop = scrollTop * 0.8;

    inner.css({
      transform: 'translateY(' + newTop + 'px)',
      height: newHeight
    });
  }

  $(document).ready(function () {
    $('.slide').addClass('is-loaded');

    $('.slideshow .arrows .arrow').on('click', function () {
      slideshowNext($(this).closest('.slideshow'), $(this).hasClass('prev'));
    });

    $('.slideshow .pagination .item').on('click', function () {
      slideshowSwitch($(this).closest('.slideshow'), $(this).index());
    });

    $('.slideshow .pagination').on('check', function () {
      var slideshow = $(this).closest('.slideshow');
      var pages = $(this).find('.item');
      var index = slideshow.find('.slides .is-active').index();
      pages.removeClass('is-active');
      pages.eq(index).addClass('is-active');
    });

    var timeout = setTimeout(function () {
      slideshowNext(slideshow, false, true);
    }, slideshowDuration);

    slideshow.data('timeout', timeout);
  });

  if ($('.main-content .slideshow').length > 1) {
    $(window).on('scroll', homeSlideshowParallax);
  }
});