// script.js
document.addEventListener('DOMContentLoaded', () => {
    const carouselImages = document.querySelectorAll('.carousel-image');

    carouselImages.forEach(image => {
        image.addEventListener('click', (e) => {
            const imageUrl = e.currentTarget.querySelector('img').src;

            const fullscreenImage = document.createElement('div');
            fullscreenImage.classList.add('fullscreen-image');

            const img = document.createElement('img');
            img.src = imageUrl;

            fullscreenImage.appendChild(img);
            document.body.appendChild(fullscreenImage);

            fullscreenImage.addEventListener('click', () => {
                fullscreenImage.remove();
            });
        });
    });
});