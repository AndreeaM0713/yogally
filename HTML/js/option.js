document.addEventListener("DOMContentLoaded", function() {
    const privateSesionBtn = document.querySelectorAll('.privateSesionBtn');
    const userSubject = document.querySelector('#userSubject');
    const options = userSubject.querySelectorAll('.subjectOption');
    const optionsObject = {};

    // Construirea obiectului optionsObject
    options.forEach((option, index) => {
        if (option.value) {
            optionsObject[`option${index}`] = {
                value: option.value,
                text: option.textContent
            };
        }
    });

    // Adaugă event listener pentru fiecare buton
    privateSesionBtn.forEach(button => {
        button.addEventListener('click', function() {
            const subject = button.getAttribute('data-subject');
            userSubject.value = subject;

            // Marcare opțiunea selectată
            options.forEach(option => {
                if (option.value === subject) {
                    option.selected = true;
                } else {
                    option.selected = false;
                }
            });
        });
    });
});
