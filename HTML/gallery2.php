<!-- GALLERY-3
============================================= -->
<div id="gallery-3" class="bg-color-02 gallery-section division">
    <div class="container">


        <!-- SECTION TITLE -->
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="section-title mb-60 text-center">

                    <!-- Transparent Header -->
                    <h2 class="tra-header txt-color-02">Galerie</h2>

                    <!-- Title 	-->
                    <h3 class="h3-xl txt-color-05">Calatoria ta incepe aici</h3>

                    <!-- Text -->
                    <p class="p-lg txt-color-05">
                        Tin sa iti multumesc pentru incredere si te felicit in avans pentru decizia de a face pasi spre eliberare.
                    </p>

                </div>
            </div>
        </div>



        <!-- PHOTOS CAROUSEL -->
        <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel gallery-photo">


                    <!-- PHOTO #1 -->
                    <div class="carousel-image">
                        <div class="hover-overlay">

                            <!-- Image -->
                            <img class="img-fluid" src="images/image1_1x1.webp" alt="gallery-image" />

                        </div>
                    </div>


                    <!-- PHOTO #2 -->
                    <div class="carousel-image">
                        <div class="hover-overlay">

                            <!-- Image -->
                            <img class="img-fluid" src="images/image2_1x1.webp" alt="gallery-image" />

                        </div>
                    </div>


                    <!-- PHOTO #3 -->
                    <div class="carousel-image">
                        <div class="hover-overlay">

                            <!-- Image -->
                            <img class="img-fluid" src="images/image3_1x1.webp" alt="gallery-image" />

                        </div>
                    </div>


                    <!-- PHOTO #4 -->
                    <div class="carousel-image">
                        <div class="hover-overlay">

                            <!-- Image -->
                            <img class="img-fluid" src="images/image4_1x1.webp" alt="gallery-image" />

                        </div>
                    </div>


                    <!-- PHOTO #5 -->
                    <div class="carousel-image">
                        <div class="hover-overlay">

                            <!-- Image -->
                            <img class="img-fluid" src="images/image5_1x1.webp" alt="gallery-image" />

                        </div>
                    </div>


                    <!-- PHOTO #6 -->
                    <div class="carousel-image">
                        <div class="hover-overlay">

                            <!-- Image -->
                            <img class="img-fluid" src="images/image6_1x1.webp" alt="gallery-image" />

                        </div>
                    </div>


                    <!-- PHOTO #7 -->
                    <div class="carousel-image">
                        <div class="hover-overlay">

                            <!-- Image -->
                            <img class="img-fluid" src="images/image7_1x1.webp" alt="gallery-image" />

                        </div>
                    </div>


                    <!-- PHOTO #8 -->
                    <div class="carousel-image">
                        <div class="hover-overlay">

                            <!-- Image -->
                            <img class="img-fluid" src="images/image8_1x1.webp" alt="gallery-image" />

                        </div>
                    </div>


                </div>
            </div>
        </div> <!-- END IMAGES CAROUSEL -->


    </div> <!-- End container -->
</div> <!-- END GALLERY-3 -->